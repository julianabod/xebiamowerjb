package com.julianabod.xebiaMower.bean;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class BeanResource {

	private static final BeanResource singleton = new BeanResource();

    public static <T> T getBean(Class<T> class1) {
        return singleton.getImpl(class1);
    }

    private <T> T getImpl(Class<T> class1) {
        @SuppressWarnings("unchecked")
        T o = (T) factory.getBean(class1.getSimpleName());
        return o;
    }

    private final BeanFactory factory;

    private BeanResource() {
        super();
        // IoC sur le terrain utilise Spring
        Resource resource = new ClassPathResource("beans.xml");
        factory = new XmlBeanFactory(resource); // fic xml de spring
    }

}
