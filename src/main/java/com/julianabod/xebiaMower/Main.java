package com.julianabod.xebiaMower;

import com.julianabod.xebiaMower.bean.BeanResource;
import static com.google.common.base.Charsets.UTF_8;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.util.StringUtils;

import com.julianabod.xebiaMower.entities.Point;
import com.google.common.collect.Lists;
import static com.google.common.io.Files.readLines;
import static com.google.common.io.Files.write;
import static javax.swing.JOptionPane.showMessageDialog;

public class Main {

	public static void main(final String[] args) {
		Main main = new Main();
		String errMsg = null;
		try {
			main.execute(args);
		} catch (Throwable e) {
			System.out.println(e.getCause()
                                +"--*--> "+e.fillInStackTrace() 
                                +" --> Message "+e.getMessage());
		} finally {
			if (errMsg != null) {
				try {
					main.result = errMsg;
					main.writeOutput();
				} catch (Exception e2) {
					System.out.println(errMsg);
				}
			}
		}

	}

	/**
	 * Fonction de traduction des param�tres en ligne de commande.
	 * 
	 * @param args  arguments transmis au programme
	 * @return Objet contenant les param�tres lus
	 */
	public static CommandLine parseCommandLine(String[] args) throws ParseException {
		// create Options object - Ajout de fonctions + param�tres
		Options options = new Options();

		options.addOption("i", "input", true, "choose an input file");
                options.addOption("o", "output", true, "choose an output file");
                options.addOption("g", "graphicinput", false, "display graphic mode to choose the input file");
                options.addOption("h", "graphicoutput", false, "display graphic mode to show the results");

		CommandLineParser parser = new GnuParser();
		CommandLine cmd = parser.parse(options, args);
		return cmd;
	}

	private boolean strictMode = false;

	/**
	 * Nom du fichier source.
	 */
	private String filename;

	/**
	 * contenu du fichier source.
	 */
	private List<String> lines;

	/**
	 * Pelouse contenant une liste de tondeuses.
	 */
	private Lawn plot;

	/**
	 * affichage des position finales des tondeuses.
	 */
	private String result;

	/**
	 * Parseur de ligne le commande.
	 */
	private CommandLine cmd;

	/**
	 * Constructeur par d�faut.
	 */
	public Main() {
	}

	/**
	 * D�place les tondeuses selon les indications de la source.
	 * 
	 * @param theLawn
	 *            Terrain
	 * @throws IOException
	 */
	public final void moveMowers(final Lawn theLawn) {
		for (Mower mower : theLawn.getTondeuses()) {
			while (mower.etapeSuivante()) {
				//Rien faire entre chaque �tape.	
			}
		}
	}

	/**
	 * Execute le programme avec les arguemnts transmis.
	 * 
	 * @param args
	 *            arguments de la ligne de commande
	 */
	public void execute(String[] args) throws ParseException {
		cmd = parseCommandLine(args);
		readArguments();
		readInput();
		loadInstructions(lines);
		moveMowers(plot);
		// pour la vue :
                formatResult();
		writeOutput();
	}

	/**
	 * Formate le r�sultat pour l'affichage textuel.
	 * 
	 * @return position format�e des tondeuses
	 */
	public final String formatResult() {
		StringBuilder sb = new StringBuilder();
		for (Mower mower : plot.getTondeuses()) {
			Point p = mower.getPoint();
			sb.append(MessageFormat.format("{0} {1} {2}\n", p.getX(), p.getY(),
					mower.getDirection().name()));
		}
		result = sb.toString();
		result = StringUtils.trimTrailingCharacter(result, '\n');
		return result;
	}

	/**
	 * Charge les instructions contenues dans le fichier source et initialise
 les objets Lawn et SimpleLawnMower.
	 * 
	 * @param theLines
	 *            instructions.
	 * @return
	 * @throws IOException
	 */
	public final void loadInstructions(final List<String> theLines) {

		// Parcours des lignes � l'aide d'un iterateur.
		Iterator<String> iterator = theLines.iterator();

		// IoC du terrain
		plot = BeanResource.getBean(Lawn.class);

		// La 1ere ligne concerne les dimensions du terrain.
		plot.initialize(iterator.next());

		// Les lignes suivantes par pairs d�crivent des tondeuses.
		while (iterator.hasNext()) {
			// Point et direction de la tondeuse
			String line = iterator.next().trim();

			try {
				Mower mower = BeanResource.getBean(Mower.class).initWith(line);

				// Instruction de d�placement (s�quence d'ordres)
				line = iterator.next().trim();
				mower.setSequenceOrdres(line);

				// Ajout de la tondeuse � la palouse
				plot.addTondeuse(mower);
			} catch (RuntimeException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Traduire les arguments en ligne de commande concernant les entr�es :
	 * langue, fichier d'entr�e, entr�e graphique.
	 * 
	 */
	private void readArguments() {
		filename = null;
		
        if (cmd.hasOption("i")) {
            filename = cmd.getOptionValue("i");
        }
        // Mode graphique pour choisir le fichier d'entr�e
        if (cmd.hasOption("g")) {
            JFileChooser chooser = new JFileChooser("./src/test/resources");
            int returnVal = chooser.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                filename = chooser.getSelectedFile().getAbsolutePath();
            }
        }
        strictMode = cmd.hasOption("s");

	}

	/**
	 * Lire la source.
	 * 
	 * @return liste des lignes du fichier
	 */
	private List<String> readInput() {
		lines = Collections.emptyList();

		if (filename == null) {
			// Pas de fichier : lecture de l'entr�e standard
			Scanner sc = new Scanner(System.in);
			lines = Lists.newArrayList(sc);
		} else {
			// Lecture du fichier
			System.out.println("Lecture du fichier"+ filename);
			try {
				lines = readLines(new File(filename), UTF_8);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		return lines;
	}

	/**
	 * Ecriture des informations sur les sorties choisies : graphique, fichier
	 * ou sortie standard.
	 */
	private void writeOutput() {
		// Par d�faut, on l'affiche sur la sortie standard
		if (cmd == null || (!cmd.hasOption("o") && !cmd.hasOption("h"))) {
			System.out.print(result);
			return;
		}
		// Affichage graphique
		if (cmd.hasOption("h")) {
			showMessageDialog(null, result);
		}

		// Sortie dans un fichier
		if (cmd.hasOption("o")) {
			File outputFile = new File(cmd.getOptionValue("o"));
			try {
				write(result, outputFile, UTF_8);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}

	}

	/**
	 * Renvoie le nom du fichier source.
	 * @return nom du fichier en entr�e
	 */
	public final String getFilename() {
		return filename;
	}

	/**
	 * Renvoie les lignes du fichier source.
	 * @return liste de lignes du fichier
	 */
	public final List<String> getLines() {
		return lines;
	}

	/**
	 * Renvoie la liste des tondeuses.
	 * @return liste des tondeuses � gazon
	 */
	public final Lawn getLawn() {
		return plot;
	}

	/**
	 * Renvoie le r�sultat affichable : position des tondeuses.
	 * @return chaine format�e contenant la position des tondeuses
	 */
	public final String getResult() {
		return result;
	}

	/**
	 * Renvoie l'�tat du mode strict.
	 * @return vrai si le mode strict est activ�
	 */
	public final boolean isStrictMode() {
		return strictMode;
	}
}
