package com.julianabod.xebiaMower;

import java.util.Collection;

import com.julianabod.xebiaMower.entities.Point;

/**
 * Interface Lawn = Pelouse 
 * @author Juliana
 *
 */
public interface Lawn {
	
    /** 
     * Liste des tondeuses se d�pla�ant sur cette pelouse
     * @return tondeuse
     */
    public void addTondeuse(Mower tondeuse);

    public Collection<Mower> getTondeuses();

    public void initialize(String line);

    /**
     * Renvoie vrai si la position indiqu�e est situ�e � l'int�rieur des
     * limites du terrain
     */
    public boolean isPositionPossible(Point point);


}
