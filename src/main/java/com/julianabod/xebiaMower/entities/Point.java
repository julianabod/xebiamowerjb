package com.julianabod.xebiaMower.entities;

public class Point {

    private int x, y;

    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }

    /**
     * Calcule la prochaine position si l'objet se d�place dans la direction indiqu�e.
     * @param direction direction vers laquelle l'objet avance.
     * @return la nouvelle position dans le futur.
     */
    public final Point nextMove(final Direction direction) {
        int newX = x + direction.getVecteurX();
        int newY = y + direction.getVecteurY();
        return new Point(newX, newY);
    }

    /**
     * Deux positions sont identiques si elles pointent sur la m�me case.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point p = (Point) obj;
            if(p.x == x && p.y == y){
            	 return true ;
            }
        } return false;
    }

    @Override
    public String toString() {
        return "Point [x=" + x + ", y=" + y + "]";
    }
}
