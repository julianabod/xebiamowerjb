package com.julianabod.xebiaMower.entities;

import java.text.MessageFormat;
import java.util.Iterator;

import com.julianabod.xebiaMower.Abstract.AbstractMower;
import com.julianabod.xebiaMower.Utils;
import com.julianabod.xebiaMower.Mower;


/**
 * Objet repr�sentant une tondeuse de type simple. Cette tondeuse r�ponds
 * uniquement aux ordres A (avancer), D (tourner � droite) et G (tourner �
 * gauche). Elle s'initialise avec 3 valeurs : abscisse, ordonn�e et direction.
 * 
 * @author Juliana
 */
public class Tondeuse extends AbstractMower {
    /**
     * Chaque tondeuse poss�de un num�ro identifiant unique pour faciliter le
     * d�buggage qui sera � faire plutard. nextNumber est le prochain identifiant � attribuer.
     */
    private static int nextNumber = 1;
    
    /**
     * Chaque tondeuse poss�de un num�ro identifiant unique pour faciliter le
     * d�buggage. currentNumber est l'identifiant de cette tondeuse.
     */
    private int currentNumber;

    /**
     * �xecute une �tape. Lis la s�quence d'ordres et �xecute l'ordre suivant.
     * @return vrai si il existe encore des ordres � �xucuter. Faux s'il n'y a
     *         plus d'ordres.
     */
    public final boolean etapeSuivante() {
        if (!sequenceOrdres.hasNext()) {
            return false;
        }
        Point oldPosition = point;
        Direction oldDirection = direction;

        char order = sequenceOrdres.next();
        switch (order) {
        case 'A':
            // La tondeuse avance d'un pas
            point = point.nextMove(direction);
            pcs.firePropertyChange("position", oldPosition, point);
            break;
        case 'D':
            // La tondeuse tourne � droite
            direction = direction.tournerADroite();
            pcs.firePropertyChange("direction", oldDirection, direction);
            break;
        case 'G':
            // La tondeuse tourne � gauche
            direction = direction.tournerAGauche();
            pcs.firePropertyChange("direction", oldDirection, direction);
            break;

        default:
            break;
        }
        return sequenceOrdres.hasNext();
    }

    /**
     * Constructeur.
     */
    public Tondeuse() {
    	currentNumber = nextNumber++;
    }

    /**
     * Initialise la point et la direction de la tondeuse avec les valeurs
 contenues dans initString.
     * 
     * @param initString
     *            chaine contenant les valeurs d'initialisation
     * @return instance courant de tondeuse
     */
    public final Mower initWith(final String initString) {
        Iterator<String> dim = Utils.split(initString);
        point = new Point(Integer.parseInt(dim.next()), Integer.parseInt(dim.next()));
        direction = Direction.valueOf(dim.next());
        return this;
    }

    /**
     * formattage pour un affichage plus lisible.
     * 
     * @return point et direction de la tondeuse
     */
    @Override
    public final String toString() {
        return MessageFormat.format("{0} {1} {2}", 
                point.getX(), point.getY(), direction.name());
    }

}
