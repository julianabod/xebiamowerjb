package com.julianabod.xebiaMower.entities;

import java.beans.PropertyChangeListener;
import java.util.Iterator;

import com.julianabod.xebiaMower.Abstract.AbstractLawn;
import com.julianabod.xebiaMower.Utils;
import com.julianabod.xebiaMower.Lawn;

public class Pelouse extends AbstractLawn implements PropertyChangeListener, Lawn {

    private int width;
    private int length;

    public Pelouse() {
        super();
    }

    /**
     * Initialise la pelouse avec ses dimensions connues.
     * @param dimension
     */
    @Override
    public void initialize(String dimension) {
        super.initialize(dimension);
        
        Iterator<String> d = Utils.split(dimension);
        length = Integer.parseInt(d.next());
        width = Integer.parseInt(d.next());
    }

    /**
     * Teste si la position transmise est bien � l'int�rieur des limites du
     * terrain.
     * @param p : point
     * @return vrai si le point sp�cifi� est situ� dans les limites de la pelouse.
     */
    
    public final boolean isPositionPossible(Point p) {
        int x = p.getX();
        int y = p.getY();
        return x >= 0 && y >= 0 && x <= length && y <= width;
    }

}
