package com.julianabod.xebiaMower.entities;

/**
 * Direction vers laquelle est tourn�e la tondeuse.
 * @author Juliana 
 */
public enum Direction {
    // E pour Est, N pour Nord, S pour Sud et W pour Ouest
    E(1, 0), N(0, 1), S(0, -1), W(-1, 0);

    private final int vecteurX;
    private final int vecteurY;

    /**
     * Constructeur
     */
    Direction(int x, int y) {
        vecteurX = x;
        vecteurY = y;
    }

    /**
     * Renvoie la valeur de d�placement sur X
     * @return incr�ment sur l'axe des abcisses
     */
    public int getVecteurX() {
        return vecteurX;
    }

    /**
     * Renvoie la valeur de d�placement sur Y
     * @return incr�ment sur l'axe des ordonn�es
     */
    public int getVecteurY() {
        return vecteurY;
    }

    /**
     * Renvoie la nouvelle direction si la tondeuse tourne �droite / gauche
     * @return nouvelle direction
     */
    public Direction tournerADroite() {
        switch (this) {
            case N:
                return E;
            case E:
                return S;
            case W:
                return N;
            case S:
                return W;
            default:
                return null;
        }
    }

    public Direction tournerAGauche() {
        switch (this) {
            case N:
                return W;
            case E:
                return N;
            case W:
                return S;
            case S:
                return E;
            default:
                return null;
        }
    }
}
