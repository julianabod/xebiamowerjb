package com.julianabod.xebiaMower.Abstract;

import com.julianabod.xebiaMower.Mower;
import static com.google.common.base.Strings.nullToEmpty;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Iterator;

import com.julianabod.xebiaMower.entities.Direction;
import com.julianabod.xebiaMower.entities.Point;

import static com.google.common.collect.Lists.charactersOf;

public abstract class AbstractMower implements PropertyChangeListener, Mower {

    /**
     * Orientation de la tondeuse.
     */
    protected Direction direction;
    /**
     * Alerte des changements de point et direction.
     */
    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    /**
     * Point de la tondeuse sur le terrain en X,Y.
     */
    protected Point point;
    /**
     * Suite des ordres de d�placement.
     */
    protected Iterator<Character> sequenceOrdres;

    /**
     * Ajoute un PropertyChangeListener � l'�coute des changements des valeurs
     * des propri�t�s de la tondeuse.
     * @param listener objet � l'�coute.
     */
    public final void addPropertyChangeListener(final PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    /**
     * Lit la s�quence d'ordres et �x�cute un ordre.
     * @return vrai s'il reste des ordre � lire
     */
    public abstract boolean etapeSuivante();

    /**
     * Renvoie la direction vers laquelle est tourn�e la tondeuse.
     * @return direction actuelle
     */
    public final Direction getDirection() {
        return direction;
    }

    /**
     * Renvoie la point de la tondeuse sur le terrain.
     * @return point actuelle.
     */
    public final Point getPoint() {
        return point;
    }

    /**
     * Compare si la tondeuse est plac� sur la m�me case du terrain.
     * @param tondeuse tondeuse � tester
     * @return vrai si les tondeuses sont au m�me endroit.
     */
    public final boolean isMemePosition(final Mower tondeuse) {
        return point.getX() == tondeuse.getPoint().getX()
                && point.getY() == tondeuse.getPoint().getY();
    }

    public final void propertyChange(final PropertyChangeEvent evt) {
        pcs.firePropertyChange(evt.getPropertyName(), evt.getOldValue(),
                evt.getNewValue());
    }

    /**
     * Supprime un PropertyChangeListener d�j� abonn�.
     * @param listener
     */
    public final void removePropertyChangeListener(final PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    /**
     * Valorise la s�quence d'ordre de d�placement sans la lire. La lecture de
     * la s�quence se fait � la vol�e.
     * @param sequenceOrdresString sc�nario contenu dans le fichier source pour cette tondeuse
     */
    public final void setSequenceOrdres(final String sequenceOrdresString) {
        String seq = nullToEmpty(sequenceOrdresString);
        sequenceOrdres = charactersOf(seq).iterator();
    }
}
