package com.julianabod.xebiaMower.Abstract;


import com.julianabod.xebiaMower.Lawn;
import com.julianabod.xebiaMower.Mower;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;


import static com.google.common.collect.Lists.newArrayList;

public abstract class AbstractLawn implements PropertyChangeListener, Lawn {

    /**
     * Liste des tondeuses pr�sentes sur ce terrain.
     */
    Collection<Mower> tondeuses = Collections.emptySet();

    /**
     * Ajout d'une nouvelle tondeuse sur le terrain. 
     */
    public void addTondeuse(Mower tondeuse) {
        //Ma tondeuse doit �tre sur le terrain bien sur.
        if (!isPositionPossible(tondeuse.getPoint())) {
        	System.out.println("Tondeuse sortie de la pelouse");
        }

        // Ajout de la tondeuse dans la liste
        tondeuses.add(tondeuse);

        // Le terrain est �l'�coute des changements de position de la tondeuse
        tondeuse.addPropertyChangeListener(this);
    }

    /**
     * Parcourir la liste des tondeuses sans modification.
     */
    public Collection<Mower> getTondeuses() {
        return tondeuses;
    }

    /**
     * Un terrain g�n�rique n'a pas de forme et ne s'initialise pas avec ce que
     * contient instruction mais cela permet de surcharger cette m�thode dans les
     * impl�mentations de terrain
     */
    public void initialize(String instruction) {
        tondeuses = newArrayList();
    }

    /**
     * Cette m�thode est appel�e lorsque quelque chose se passe sur le terrain.
     * Pour le moment, les seuls �v�nements qui se produisent sont des
     * d�placements de tondeuses.
     * 
     * @param evt
     *            un objet PropertyChangeEvent contenant la tondeuse source.
     * @throws TondeuseException
     *             La tondeuse est sortie du terrain.
     * @throws SamePositionException
     *             2 tondeuses se sont rencontr�es provocant un accident.
     */
    public void propertyChange(PropertyChangeEvent evt) {

        // ne r�agir qu'au changement de position
        if (!"position".equals(evt.getPropertyName()))
            return;
        Mower tondeuse = (Mower) evt.getSource();
        if (tondeuse == null)
            return;
        
        // V�rifie si toujours sur le terrain
        if (!isPositionPossible(tondeuse.getPoint())) {
        	System.out.println("ATTENTION : Tondeuse hors de la pelouse");
        }

        // V�rifie si choc entre tondeuses
        for (Mower tondeuse2 : tondeuses) {
            if (tondeuse != tondeuse2 && tondeuse.isMemePosition(tondeuse2)) {
                System.out.println("ATTENTION : choc de tondeuses");
                // throw new SamePositionException(tondeuse, tondeuse2);
            }
        }
    }
}
