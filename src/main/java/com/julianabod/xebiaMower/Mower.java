package com.julianabod.xebiaMower;

import com.julianabod.xebiaMower.entities.Direction;
import com.julianabod.xebiaMower.entities.Point;

import java.beans.PropertyChangeListener;

/**
 * Interface Mower
 * @author Juliana
 */
public interface Mower {

    /**
     * Lit la s�quence d'ordres et �x�cute un ordre.
     * @return vrai s'il reste des ordres � lire
     */
    public abstract boolean etapeSuivante();

    /**
     * Renvoie la direction vers laquelle est tourn�e la tondeuse.
     * @return direction actuelle
     */
    public Direction getDirection();

    /**
     * Renvoie la position de la tondeuse sur le terrain.
     * @return position actuelle.
     */
    public Point getPoint();

    /**
     * Initialise la position et la direction de la tondeuse avec les valeurs
     * contenues dans initString.
     * @param initString
     *            chaine contenant les valeurs d'initialisation
     */
    public abstract Mower initWith(String initString);

    public abstract void setSequenceOrdres(String string); //Instructions

    public abstract void addPropertyChangeListener(PropertyChangeListener propertyChangeListener);
        //event ^
    public abstract boolean isMemePosition(Mower tondeuse2);
}
