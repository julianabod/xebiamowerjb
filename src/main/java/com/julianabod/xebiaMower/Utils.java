package com.julianabod.xebiaMower;

import java.util.Iterator;
import java.util.Map;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

/**
 * Classe utilitaire pour l'application.
 * 
 * @author Juliana
 */
public final class Utils {
    /**
     * D�coupeur de chaine utlisant le caract�re espace comme s�parateur.
     */
    private static final Splitter splitEspace = Splitter.on(' ').trimResults().omitEmptyStrings();

    /**
     * D�couper une chaine le caract�re espace comme s�parateur.
     * 
     * @param str
     * @return It�rateur sur le r�sultat du d�coupage
     */
    public static Iterator<String> split(final String str) {
        return splitEspace.split(str).iterator();
    }

    /**
     * Constructeur cach� interdisant d'instancier cette classe.
     */
    private Utils() {
    }

    public static String mapToString(Map<?, ?> myContentMap) {
        return Joiner.on("\n").withKeyValueSeparator("=").join(myContentMap);
    }
}
