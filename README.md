## **// Readme //** ##

### Environnements techniques ###
*  Langage de programmation: Java, xml
* Dans une version n+1, je tenterai d'additionner ce projet, à une structure MVC (jva/flex) "Je sais c'est obselète, mais pourquoi pas essayer"
*  Gestionnaire de source: Git - repository : https://bitbucket.org/julianabod/xebiamowerjb (dépot privée -> M.Laroche est invité à lire)
*  Gestionnaire de projet: Maven
*  IDE: Netbeans 8.1 (mac)

### Indications ###
*  Prise en charge d'un fichier.txt (pour le développement, utilisation du fichier : fic.txt (TEST disponible dans l'énoncé du projet)
* Possibilités d'afficher le resultat en console, ou via une interface swing

### Etapes ###
* Copier le dossier du projet en local
* Lancer la console
* Dans le dossier du projet - Supprimer le fichier target
* taper : mvn package
* taper : cd target/
* taper : java -jar xebiaMowerJB-jar-with-dependencies.jar -g
* sur l'interface swing, récupèrer le fichier.txt contenant les deux lignes (par tondeuse).
* Observer le résultat sur la console






-> Juliana BOD